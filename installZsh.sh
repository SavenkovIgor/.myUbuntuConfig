ZSH_PACK="zsh"
ZSH_FONTS="powerline fonts-powerline"
ZSH_THEME="zsh-theme-powerlevel9k"
ZSH_TOOLS="zsh-syntax-highlighting"

sudo apt install -y $ZSH_PACK $ZSH_FONTS $ZSH_THEME $ZSH_TOOLS

cp ./.zshrc ~/.zshrc

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --keep-zshrc

cd ~/.oh-my-zsh/custom/plugins
git clone https://github.com/jocelynmallon/zshmarks.git
